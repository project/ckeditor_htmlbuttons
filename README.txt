
This module enables the feature to add custom html snippet buttons to the
CKEditor toolbar.

Install
-------
1. Enable "CKEditor HTML Buttons" module
2. Add new button: admin/config/content/ckeditor-htmlbuttons
3. a) WYSIWYG: Enable the newly created button from WYSIWYG profiles:
      admin/config/content/wysiwyg
   b) CKEDITOR: Enable the newly created button from CKEditor configuration
      page admin/config/content/ckeditor. Under "Editor appearance" drag the
      button to active toolbar and in the same section enable "HTML Buttons".

Dependencies
------------
* Wysiwyg (https://www.drupal.org/project/wysiwyg) or
  CKEditor (https://www.drupal.org/project/ckeditor).
